package vn.chungbd.demointegrationlib

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import ncis.sg.pwa_sdk.activity.PWAActivity
import ncis.sg.pwa_sdk.delegate.NCISVersionChecker

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViewById<Button>(R.id.button).apply {
            setOnClickListener {
                checkingVersion()
            }
        }
    }

    val checker = NCISVersionChecker(
        urlForChecking = "https://demo-web-content.azurewebsites.net/"
    )

    private fun checkingVersion() {
        checker.process { version ->
            Log.i("MainActivity", "process $version")
            version?.let {
                if (it.versionNum > 0) {
                    checker.downloadFile(this, it.downloadUrl) { percent, file ->
                        Log.i("MainActivity", "percent $percent file $file")
                        if (file != null) {
                            moveToPWAActivity(file.path)
                        }
                    }
                }
            }
        }
    }

    private fun moveToPWAActivity(urlOfContentFile: String) {
        val home = PWAActivity.getIntent(
            this,
            filePath = urlOfContentFile,
            port = 8080
        )
        startActivity(home)
    }
}

