## Setup 

1. The SDK is available at [release](https://gitlab.com/chungvotinh/ncis-android-sdk/tree/master/release).  

    In the app's build.gradle, add the following dependencies:
    ```gradle
    dependencies {
        implementation files('lib/pwa-sdk.aar')
    
        implementation 'androidx.constraintlayout:constraintlayout:2.1.3'
        implementation 'androidx.webkit:webkit:1.4.0'
        implementation 'com.google.code.gson:gson:2.9.0'
        implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-android:1.6.1-native-mt'
        implementation "androidx.work:work-runtime-ktx:2.7.1"
        implementation 'com.google.firebase:firebase-messaging-ktx:23.0.4'

        implementation 'com.squareup.retrofit2:retrofit:2.6.3'
        implementation 'com.squareup.retrofit2:converter-gson:2.6.3'

        //ViewModel
        implementation "androidx.lifecycle:lifecycle-viewmodel:$lifecycle_version"
        implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycle_version"

        coreLibraryDesugaring("com.android.tools:desugar_jdk_libs:1.1.5")

        implementation 'org.jetbrains.kotlin:kotlin-stdlib-jdk8:1.6.21'
        implementation "io.ktor:ktor-server-call-logging:2.0.3"
        implementation 'ch.qos.logback:logback-classic:1.2.11'
        implementation 'io.ktor:ktor-server-cio:2.0.3'
    }
    ```
2. Sync Gradle and/or build your application

## API Documentation

## Checking Version
In your application's Application class:


```kotlin
    val checker = NCISVersionChecker(
        urlForChecking = "api-url"
    )

    checker.process { version ->
        Log.i("MainActivity", "process $version")
        version?.let {
            if (it.versionNum > 0) {
                checker.downloadFile(this, it.downloadUrl) { percent, file ->
                    Log.i("MainActivity", "percent $percent file $file")
                    if (file != null) {
                        moveToPWAActivity(file.path)
                    }
                }
            }
        }
    }

```

## Showing Web Content Activity

```kotlin
    private fun moveToPWAActivity(urlOfContentFile: String) {
        val home = PWAActivity.getIntent(
            this,
            filePath = urlOfContentFile,
            port = 8080
        )
        startActivity(home)
    }
```
